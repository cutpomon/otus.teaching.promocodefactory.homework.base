﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public class AppMappingProfile : Profile
    {
        public AppMappingProfile()
        {
            CreateMap<CreateEmployeeRequest, Employee>()
                .ForMember(dest => dest.Roles, opt => opt.Ignore());

            CreateMap<UpdateEmployeeRequest, Employee>()
                .ForMember(dest => dest.Roles, opt => opt.Ignore());
        }
    }
}
