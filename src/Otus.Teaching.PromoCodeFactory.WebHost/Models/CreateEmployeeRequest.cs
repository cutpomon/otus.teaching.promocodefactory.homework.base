﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CreateEmployeeRequest
    {
        [Required(ErrorMessage = "FirstName обязательно для заполнения")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "LastName обязательно для заполнения")]
        public string LastName { get; set; }

        [EmailAddress(ErrorMessage = "Email введен некорректно")]
        public string Email { get; set; }

        public ICollection<Guid> RoleIds { get; set; }

        public int AppliedPromocodesCount { get; set; } = 0;
    }
}
