﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class UpdateEmployeeRequest
    {
        [Required(ErrorMessage = "FirstName обязательно для заполнения")]
        public string FirstName { get; init; }

        [Required(ErrorMessage = "LastName обязательно для заполнения")]
        public string LastName { get; init; }

        [EmailAddress(ErrorMessage = "Email введен некорректно")]
        public string Email { get; init; }

        public ICollection<Guid> RoleIds { get; set; }

        public int AppliedPromocodesCount { get; init; } = 0;
    }
}
