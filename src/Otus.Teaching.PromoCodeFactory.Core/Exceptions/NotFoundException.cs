﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}
