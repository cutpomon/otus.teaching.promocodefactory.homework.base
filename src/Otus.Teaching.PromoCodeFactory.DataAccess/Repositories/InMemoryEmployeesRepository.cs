﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryEmployeesRepository
        : IRepository<Employee>
    {
        protected ICollection<Employee> Data { get; set; }

        public InMemoryEmployeesRepository(ICollection<Employee> data)
        {
            Data = data;
        }
        
        public Task<ICollection<Employee>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<Employee> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> AddAsync(Employee entity)
        {
            entity.Id = Guid.NewGuid();
            Data.Add(entity);

            return Task.FromResult(entity.Id);
        }

        public Task DeleteAsync(Guid id)
        {
            var entityToDelete = Data.FirstOrDefault(x => x.Id == id)
                ?? throw new NotFoundException("Сотрудник не найден");

            Data.Remove(entityToDelete);

            return Task.CompletedTask;
        }

        public Task UpdateAsync(Guid id, Employee newEntity)
        {
            var oldEntity = Data.FirstOrDefault(x => x.Id == id)
                ?? throw new NotFoundException("Сотрудник не найден");

            newEntity.Id = oldEntity.Id;

            Data.Remove(oldEntity);
            Data.Add(newEntity);

            return Task.CompletedTask;
        }
    }
}