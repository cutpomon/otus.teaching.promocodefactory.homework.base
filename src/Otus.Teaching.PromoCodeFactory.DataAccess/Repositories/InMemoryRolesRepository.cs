﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRolesRepository
        : IRepository<Role>
    {
        protected ICollection<Role> Data { get; set; }

        public InMemoryRolesRepository(ICollection<Role> data)
        {
            Data = data;
        }
        
        public Task<ICollection<Role>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<Role> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> AddAsync(Role entity)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(Guid id, Role newEntity)
        {
            throw new NotImplementedException();
        }
    }
}